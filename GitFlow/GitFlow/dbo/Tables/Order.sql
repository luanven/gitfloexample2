﻿CREATE TABLE [dbo].[Order] (
    [orderID]     INT      NOT NULL,
    [orderDateee] DATETIME NULL,
    [customerID]         INT      NULL,
    CONSTRAINT [PK_Order] PRIMARY KEY CLUSTERED ([orderID] ASC),
    CONSTRAINT [FK_Order_Customer] FOREIGN KEY ([cID]) REFERENCES [dbo].[Customer] ([CustomerID])
);

