﻿CREATE TABLE [dbo].[Address]
(
	[Id] INT NOT NULL PRIMARY KEY,
	[Name] VARCHAR(255) NOT NULL,
	[Address1] VARCHAR(100) NOT NULL,
	[Address2] VARCHAR(100) NULL,
	[City] VARCHAR(100) NOT NULL,
	[State] VARCHAR(3) NOT NULL,
	[CountryId] INT NOT NULL,
	[CustomerId] INT NULL, 
    CONSTRAINT [FK_CustomerId_Customer] FOREIGN KEY ([CustomerId]) REFERENCES [Customer]([CustomerID])
)


