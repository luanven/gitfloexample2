﻿CREATE TABLE [dbo].[Customer] (
    [CustomerID]      INT           NOT NULL,
    [custName]        VARCHAR (500) NOT NULL,
    [CustLastName] INT           NULL,
    CONSTRAINT [PK_Customer] PRIMARY KEY CLUSTERED ([CustomerID] ASC)
);

