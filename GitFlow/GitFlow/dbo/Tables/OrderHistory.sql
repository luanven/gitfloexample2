﻿CREATE TABLE [dbo].[OrderHistory]
(
	[Id] INT NOT NULL PRIMARY KEY, 
    [OrderID] INT NULL, 
    [Comment] NVARCHAR(50) NULL, 
    [DateStamp] TIMESTAMP NULL, 
    CONSTRAINT [FK_OrderHistory_ToTable] FOREIGN KEY (OrderID) REFERENCES [Order]([orderID])
)
